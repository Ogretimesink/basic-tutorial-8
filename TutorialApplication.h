/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.h
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
 
class TutorialApplication : public BaseApplication
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);
 
protected:
	// Function to fill the scene with graphic objects
	virtual void createScene(void);

	// Function to initialize the scene manager object
	virtual void chooseSceneManager(void);

	// Function to initialize the scene camera
	virtual void createCamera(void);

	// Function to initialize the scene viewports
	virtual void createViewports(void);

	// Function to initialize input objects
	virtual void createFrameListener(void);
 
	// Function to update scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
 
	// Function to proces keyboard input
	virtual bool keyPressed( const OIS::KeyEvent &arg );

	// Function to proces keyboard input
	virtual bool keyReleased( const OIS::KeyEvent &arg );

	// Function to proces mouse input
	virtual bool mouseMoved( const OIS::MouseEvent &arg );

	// Function to proces mouse input
	virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

	// Function to proces mouse input
	virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
 
private:
	// Primary manager object that controls all things rendered on the screen
	Ogre::SceneManager* mPrimarySceneMgr;

	// Secondary manager object that controls all things rendered on the screen
	Ogre::SceneManager* mSecondarySceneMgr;

	// Flag to make a dual viewport view
	bool mDual;
 
	// Function to show only 1 viewport at once
	virtual void setupViewport(Ogre::SceneManager *curr);

	// Function to show 2 viewport at once
	virtual void dualViewport(Ogre::SceneManager *primarySceneMgr, Ogre::SceneManager *secondarySceneMgr);
};
//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------