/*
-----------------------------------------------------------------------------
Filename:	TutorialApplication.cpp
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

// Declare a preprocessor directive for the camera name
#define CAMERA_NAME "SceneCamera"

//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
	: mPrimarySceneMgr(0)
	, mSecondarySceneMgr(0)
	, mDual(false)
{
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//-------------------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Add a sky box to the scene
	mPrimarySceneMgr->setSkyBox(true, "Examples/SpaceSkyBox");

	// Add a sky dome to the scene
	mSecondarySceneMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);
}
//---------------------------------------------------------------------------
void TutorialApplication::chooseSceneManager(void)
{
	// Initialize the primary scene manager object
	mPrimarySceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC, "primary");

	// Initialize the secondary scene manager object
	mSecondarySceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC, "secondary");
}
//---------------------------------------------------------------------------
void TutorialApplication::createCamera()
{
	// Initialize the scene camera for the primary scene manager
	mPrimarySceneMgr->createCamera(CAMERA_NAME);

	// Initialize the scene camera for the secondary scene manager
	mSecondarySceneMgr->createCamera(CAMERA_NAME);
}
//---------------------------------------------------------------------------
void TutorialApplication::createViewports()
{
	// Call function to initialize the scene viewports
	setupViewport(mPrimarySceneMgr);
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Write message to log about the start of the initialization of input devices
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

	// Create a list of parameters
	OIS::ParamList pl;

	// Declare unsigned integer for the window handle
	size_t windowHnd = 0;

	// Declare stream to name for the window handle
	std::ostringstream windowHndStr;
 
	// Get the window handle
	mWindow->getCustomAttribute("WINDOW", &windowHnd);

	// Bitwise left shift?? 
	windowHndStr << windowHnd;

	// Pass the name and window handle as a pair to the list
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
 
	// Create the input device manager
	mInputManager = OIS::InputManager::createInputSystem( pl );
 
	// Create the keyboard input device object
	mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject( OIS::OISKeyboard, true ));

	// Create the mouse input device object
	mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject( OIS::OISMouse, true ));
 
	// Register as a mouse listener
	mMouse->setEventCallback(this);

	// Register as a keyboard listener
	mKeyboard->setEventCallback(this);
 
	// Set initial mouse clipping size
	windowResized(mWindow);
 
	// Register as a Window listener
	Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
 
	// Add this function createFrameListener as a frame listener
	mRoot->addFrameListener(this);
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	// Check the windows status
	if(mWindow->isClosed())

		// End the application
		return false;
 
	// Check the application shutdown flag
	if(mShutDown)

		// End the application
		return false;
 
	// Check the keyboard input device
	mKeyboard->capture();

	// Check the mouse input device
	mMouse->capture();
 
	return true;
}
//-------------------------------------------------------------------------------------
void TutorialApplication::setupViewport(Ogre::SceneManager *curr)
{
	// Delete all viewports in the window
	mWindow->removeAllViewports();

	// Get the current scene manager camera by name
	Ogre::Camera *cam = curr->getCamera(CAMERA_NAME);

	// Declare and initialize a scene rendering viewpoint for the camera
	Ogre::Viewport *vp = mWindow->addViewport(cam);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

	// Set the viewport size
	cam->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//---------------------------------------------------------------------------
void TutorialApplication::dualViewport(Ogre::SceneManager *primarySceneMgr, Ogre::SceneManager *secondarySceneMgr)
{
	// Delete all viewports in the window
	mWindow->removeAllViewports();
 
	// Declare and initialize a scene rendering viewpoint for the camera
	Ogre::Viewport *vp = 0;

	// Get the primary scene manager camera by name
	Ogre::Camera *cam = primarySceneMgr->getCamera(CAMERA_NAME);

	// Declare and initialize a scene rendering viewpoint for the camera
	vp = mWindow->addViewport(cam, 0, 0, 0, 0.5, 1);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

	// Set the viewport size
	cam->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
 
	// Get the secondary scene manager camera by name
	cam = secondarySceneMgr->getCamera(CAMERA_NAME);

	// Declare and initialize a scene rendering viewpoint for the camera
	vp = mWindow->addViewport(cam, 1, 0.5, 0, 0.5, 1);

	// Set the scene background color
	vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

	// Set the viewport size
	cam->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//---------------------------------------------------------------------------
static void swap(Ogre::SceneManager *&first, Ogre::SceneManager *&second)
{
	// Declare and initialize a temporary scene manager to store the first scene manager
	Ogre::SceneManager *tmp = first;

	// Switch the secondary scene manager for the primary
	first = second;

	// Switch the secondary scene manager for the temporary
	second = tmp;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed( const OIS::KeyEvent &arg )
{
	// Check for the escape key
	if (arg.key == OIS::KC_ESCAPE){
		// Set the flag to shutdown the applicaion
		mShutDown = true;
	}
	// Check for the V key
	else if(arg.key == OIS::KC_V)
	{
		// Toggle the dual viewport view flag
		mDual = !mDual;
 
		// Check the dual viewport flag
		if (mDual)
		{
			// Call the function to show two viewports at once
			dualViewport(mPrimarySceneMgr, mSecondarySceneMgr);
		}
		else
		{
			// Call the function to show only one viewport
			setupViewport(mPrimarySceneMgr);
		}
	}
	// Check for the C key
	else if(arg.key == OIS::KC_C)
	{
		// Call the function to switch the primary and secondary scene managers
		swap(mPrimarySceneMgr, mSecondarySceneMgr);
 
		// Check the dual viewport flag
		if (mDual)
		{
			// Call the function to show two viewports at once
			dualViewport(mPrimarySceneMgr, mSecondarySceneMgr);
		}
		else
		{
			// Call the function to show only one viewport
			setupViewport(mPrimarySceneMgr);
		}
	}
 
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased( const OIS::KeyEvent &arg )
{
	// Ignore keyboard input
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved( const OIS::MouseEvent &arg )
{
	// Ignore mouse input
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	// Ignore mouse input
	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	// Ignore mouse input
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	// For windows playtform
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	// For linux and apple platform
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			// Display windows error message box
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			// Display error message to linux and apple standard output error stream
			std::cerr << "An exception has occurred: " << e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
